let btn = document.getElementById("btn");
let textArea = document.querySelector(".textArea");
btn.addEventListener("click", () => {
  setTimeout(() => {
    textArea.textContent = "Операція успішна";
  }, 3000);
});

let textArea2 = document.querySelector(".textArea2");
document.addEventListener("DOMContentLoaded", () => {
  let timeout = setInterval(() => {
    let counter = textArea2.textContent;
    counter--;
    textArea2.textContent = counter;
    if (counter === 0) {
      clearInterval(timeout);
      textArea2.textContent = "Зворотній відлік завершено";
    }
  }, 1000);
});

